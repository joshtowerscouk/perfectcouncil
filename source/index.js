import 'normalize.css'
import './style/main.scss'

import 'slick-carousel'

import $ from 'jquery'
import matchHeight from 'jquery-match-height'

$(document).ready(function(){
	$('#main-menu-toggle').on('click', function(){
		$(this).toggleClass('open')
		$('#main-menu-mobile').toggleClass('open')
		toggleAriaHidden('#main-menu-mobile')
	})

	$('.services-carousel').slick({
		dots: false,
		infinite: true,
		speed: 300,
		slidesToShow: 4,
		slidesToScroll: 1,
		responsive: [
			{
				breakpoint: 1024,
				settings: {
					slidesToShow: 3
				}
			},
			{
				breakpoint: 760,
				settings: {
					slidesToShow: 2
				}
			},
			{
				breakpoint: 600,
				settings: {
					slidesToShow: 1
				}
			}
		]
	});

	$('.news-events__carousel').slick({
		dots: false,
		infinite: true,
		speed: 300,
		slidesToShow: 3,
		slidesToScroll: 1,
		responsive: [
			{
				breakpoint: 1024,
				settings: {
					slidesToShow: 2
				}
			},
			{
				breakpoint: 640,
				settings: {
					slidesToShow: 1
				}
			}
		]
	});

	$('.match').matchHeight()

	$('.hero__scroll-down').on('click', function(){
		scrollPageTo('.council-services', 0)
	})

	$('.council-services__toggles__single').on('click', function(){
		let $this = $(this),
		$subNav = $('.council-services__toggles__subnav'),
		$otherToggleSingle = $('.council-services__toggles__single.open'),
		$allSubNavs = $('.council-services__toggles__subnav__list'),
		activeSubNavID = $this.data('cs-toggle'),
		$activeSubNav = $('.council-services__toggles__subnav__list[data-cs-subnav="' + activeSubNavID + '"]')

		if($this.hasClass('open')) {
			$this.removeClass('open')
			$subNav.removeClass('open')
			$activeSubNav.hide()
			return
		} else {
			$otherToggleSingle.removeClass('open')
			$allSubNavs.hide()
			$(this).addClass('open')
			$subNav.addClass('open')
			$activeSubNav.show()
		}

		if($(window).width() < 801) {
			scrollPageTo('.council-services__toggles__subnav', -100)
		}

	})

})

const scrollPageTo = (elem, offset) => {
	let $elementToScrollTo = $(elem)
	$('html,body').unbind().animate({ scrollTop: $elementToScrollTo.offset().top + offset }, 'slow')
}


const toggleHidden = (id) => {
	let $elementToToggle = $(id)
	if ($elementToToggle.attr('aria-hidden') == 'true') {
		$elementToToggle.attr('aria-hidden', 'false')
	} else {
		$elementToToggle.attr('aria-hidden', 'true')
	}
}